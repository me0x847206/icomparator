/*
 * Copyright (C) 2017 Teodor MAMOLEA <Teodor.Mamolea@gmail.com>
 *
 * ******************************************************************************
 *
 * DOWHATYOUWANTTODO
 *
 * ******************************************************************************
 */
package com.me0x.icomparator;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import static java.awt.Color.GRAY;
import static java.awt.Color.GREEN;
import static java.awt.Color.ORANGE;

import javax.swing.JPanel;

/**
 * @author Teodor MAMOLEA
 */
class Pixel extends JPanel implements MouseListener, MouseMotionListener {

    private static final Color ACTIVE_COLOR = GREEN;
    private static final Color INACTIVE_COLOR = GRAY;
    private static final Color CLONE_COLOR = ORANGE;

    private boolean state;
    private Pixel clone;

    Pixel() {
        super();

        setBackground(INACTIVE_COLOR);
        addMouseListener(this);
    }

    int getStatus() {
        return state ? 1 : 0;
    }

    void setState(final boolean state) {
        this.state = state;
        setBackground(state ? ACTIVE_COLOR : INACTIVE_COLOR);
    }

    void setClone(final Pixel clone) {
        this.clone = clone;
    }

    private void setClone(final boolean b) {
        setBackground(b ? CLONE_COLOR : (state ? ACTIVE_COLOR : INACTIVE_COLOR));
    }

    @Override
    public void mouseClicked(final MouseEvent e) {
        state = !state;
        setBackground(state ? ACTIVE_COLOR : INACTIVE_COLOR);
    }

    @Override
    public void mousePressed(final MouseEvent e) {
    }

    @Override
    public void mouseReleased(final MouseEvent e) {
    }

    @Override
    public void mouseEntered(final MouseEvent e) {
        clone.setClone(true);
        setBackground(CLONE_COLOR);
    }

    @Override
    public void mouseExited(final MouseEvent e) {
        clone.setClone(false);
        setBackground(state ? ACTIVE_COLOR : INACTIVE_COLOR);
    }

    @Override
    public void mouseDragged(final MouseEvent e) {
    }

    @Override
    public void mouseMoved(final MouseEvent e) {
    }
}
