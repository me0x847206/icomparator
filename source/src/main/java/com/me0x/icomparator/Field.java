/*
 * Copyright (C) 2017 Teodor MAMOLEA <Teodor.Mamolea@gmail.com>
 *
 * ******************************************************************************
 *
 * DOWHATYOUWANTTODO
 *
 * ******************************************************************************
 */
package com.me0x.icomparator;

import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * @author Teodor MAMOLEA
 */
strictfp class Field extends JPanel {

    private String name;

    private JTextField textField;
    private JLabel label;

    Field(final String name, final String toolTip) {
        super();

        this.name = name;

        guiInit();

        textField.setToolTipText(toolTip);
    }

    private void guiInit() {
        this.setLayout(new FlowLayout());

        label = new JLabel(name + " = ");
        textField = new JTextField("0", 12);
        textField.setEditable(false);

        add(label);
        add(textField);
    }

    @Override
    public String getName() {
        return name;
    }

    void setValue(final String value) {
        textField.setText(value);
    }
}
