/*
 * Copyright (C) 2017 Teodor MAMOLEA <Teodor.Mamolea@gmail.com>
 *
 * ******************************************************************************
 *
 * DOWHATYOUWANTTODO
 *
 * ******************************************************************************
 */
package com.me0x.icomparator;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static java.awt.BorderLayout.NORTH;
import static java.awt.BorderLayout.WEST;
import static java.awt.Color.GRAY;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * @author Teodor MAMOLEA
 */
strictfp class ImagePanel extends JPanel implements ActionListener {

    private String name = "";
    private final int w;
    private final int h;
    private Pixel[][] pixels;

    ImagePanel(final String name, final int h, final int w) {
        this.name = name;
        this.w = w;
        this.h = h;

        pixels = new Pixel[h][w];

        for (int i = 0, j; i < h; ++i) {
            for (j = 0; j < w; ++j) {
                pixels[i][j] = new Pixel();
            }
        }

        guiInit();
    }

    private void guiInit() {
        final JPanel pixelsPanel = new JPanel();
        final JPanel topPanel = new JPanel();
        final JPanel horizontalPanel = new JPanel();
        final JPanel verticalPanel = new JPanel();
        final JButton clearButton = new JButton("Clear all");
        final JButton selectButton = new JButton("Select all");
        final GridLayout layout = new GridLayout(h, w);

        clearButton.addActionListener(this);
        selectButton.addActionListener(this);
        layout.setHgap(1);
        layout.setVgap(1);

        setLayout(new BorderLayout());
        pixelsPanel.setLayout(layout);
        verticalPanel.setLayout(new GridLayout(h, 1));
        horizontalPanel.setLayout(new GridLayout(1, w + 1));
        topPanel.setLayout(new FlowLayout());

        for (int i = 0; i < h; ++i) {
            final JLabel label = new JLabel(" " + (i + 1) + " ");
            label.setForeground(GRAY);
            verticalPanel.add(label);
            for (int j = 0; j < w; ++j) {
                pixelsPanel.add(pixels[i][j]);
            }
        }

        horizontalPanel.add(new JLabel(" "));
        for (int j = 0; j < w; ++j) {
            final JLabel label = new JLabel(" " + (j + 1) + " ");
            label.setForeground(GRAY);
            horizontalPanel.add(label);
        }

        topPanel.add(new JLabel(name));
        topPanel.add(selectButton);
        topPanel.add(clearButton);

        final JPanel centerPanel = new JPanel();
        centerPanel.setLayout(new BorderLayout());

        centerPanel.add(pixelsPanel);
        centerPanel.add(verticalPanel, WEST);
        centerPanel.add(horizontalPanel, NORTH);

        add(centerPanel);
        add(topPanel, NORTH);
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        final String from = ((JButton) event.getSource()).getText();
        final boolean newState;

        if ("Clear all".equals(from)) {
            newState = false;
        } else if ("Select all".equals(from)) {
            newState = true;
        } else {
            return;
        }

        for (int i = 0, j; i < h; ++i) {
            for (j = 0; j < w; ++j) {
                pixels[i][j].setState(newState);
            }
        }
    }

    static void setClone(final ImagePanel a, final ImagePanel b) {
        for (int i = 0, j; i < a.h; ++i) {
            for (j = 0; j < a.w; ++j) {
                a.pixels[i][j].setClone(b.pixels[i][j]);
                b.pixels[i][j].setClone(a.pixels[i][j]);
            }
        }
    }

    static float[] calculateABCD(final ImagePanel a, final ImagePanel b) {
        final float[] data = new float[4];

        data[0] = data[1] = data[2] = data[3] = (float) 0.0;

        for (int i = 0, j; i < a.h; ++i) {
            for (j = 0; j < a.w; ++j) {
                data[0] += a.pixels[i][j].getStatus() * b.pixels[i][j].getStatus();
                data[1] += (1 - a.pixels[i][j].getStatus()) * (1 - b.pixels[i][j].getStatus());
                data[2] += a.pixels[i][j].getStatus() * (1 - b.pixels[i][j].getStatus());
                data[3] += (1 - a.pixels[i][j].getStatus()) * b.pixels[i][j].getStatus();
            }
        }

        return data;
    }
}
