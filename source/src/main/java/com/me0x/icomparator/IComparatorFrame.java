/*
 * Copyright (C) 2017 Teodor MAMOLEA <Teodor.Mamolea@gmail.com>
 *
 * ******************************************************************************
 *
 * DOWHATYOUWANTTODO
 *
 * ******************************************************************************
 */
package com.me0x.icomparator;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

/**
 * @author Teodor MAMOLEA
 */
strictfp class IComparatorFrame extends JFrame implements ActionListener {

    private static final int IMAGE_WIDTH = 8;
    private static final int IMAGE_HEIGHT = 8;

    private final ImagePanel imageA, imageB;

    private Field a;
    private Field b;
    private Field c;
    private Field d;
    private Field s1;
    private Field s2;
    private Field s3;
    private Field s4;
    private Field s5;
    private Field s6;
    private Field s7;

    IComparatorFrame() {
        super("Window frame");

        imageA = new ImagePanel("Image A", IMAGE_HEIGHT, IMAGE_WIDTH);
        imageB = new ImagePanel("Image B", IMAGE_HEIGHT, IMAGE_WIDTH);

        ImagePanel.setClone(imageA, imageB);

        guiInit();

        setVisible(true);
    }

    private void guiInit() {
        final JPanel imagesPanel = new JPanel();
        final JPanel bottomPanel = new JPanel();
        final JButton updateButton = new JButton("Update DATA");
        final GridLayout layout = new GridLayout(1, 2);

        layout.setVgap(2);
        layout.setHgap(10);
        imagesPanel.setLayout(layout);
        bottomPanel.setLayout(new BorderLayout());
        updateButton.addActionListener(this);

        imagesPanel.add(imageA);
        imagesPanel.add(imageB);

        final JPanel dataPanel = new JPanel();
        dataPanel.setLayout(new GridLayout(4, 3));

        a = new Field("a", "Numarul de pixeli comuni");
        s1 = new Field("S1", "Russel si Rao");
        s5 = new Field("S5", "Sokal si Michener");
        b = new Field("b", "Numarul de pixeli diferiti");
        s2 = new Field("S2", "Zhokara si Nidmena");
        s6 = new Field("S6", "Kulzhinskogo");
        c = new Field("c", "Numarul de pixeli selectati in A si neselectati in B");
        s3 = new Field("S3", "Dice");
        s7 = new Field("S7", "Yule");
        d = new Field("d", "Numarul de pixeli neselectati in A si selectati in B");
        s4 = new Field("S4", "Snifa si  Sokal");

        dataPanel.add(a);
        dataPanel.add(s1);
        dataPanel.add(s5);
        dataPanel.add(b);
        dataPanel.add(s2);
        dataPanel.add(s6);
        dataPanel.add(c);
        dataPanel.add(s3);
        dataPanel.add(s7);
        dataPanel.add(d);
        dataPanel.add(s4);

        bottomPanel.add(updateButton, BorderLayout.NORTH);
        bottomPanel.add(dataPanel);

        add(imagesPanel);
        add(bottomPanel, BorderLayout.SOUTH);

        setSize(new Dimension(1000, 600));
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    @Override
    public void actionPerformed(final ActionEvent event) {
        final float[] data = ImagePanel.calculateABCD(imageA, imageB);

        a.setValue("" + (int) data[0]);
        b.setValue("" + (int) data[1]);
        c.setValue("" + (int) data[2]);
        d.setValue("" + (int) data[3]);

        final float n = (float) (IMAGE_HEIGHT * IMAGE_WIDTH * 1.0);
        s1.setValue("" + (data[0] / n));

        try {
            s2.setValue("" + (data[0] / (n - data[1])));
        } catch (final ArithmeticException e) {
            s2.setValue(" - ");
        }

        try {
            s3.setValue("" + (data[0] / (2 * data[0] + data[2] + data[3])));
        } catch (final ArithmeticException e) {
            s3.setValue(" - ");
        }

        try {
            s4.setValue("" + (data[0] / (data[0] + 2 * (data[2] + data[3]))));
        } catch (final ArithmeticException e) {
            s4.setValue(" - ");
        }

        try {
            s5.setValue("" + ((data[0] + data[1]) / n));
        } catch (final ArithmeticException e) {
            s5.setValue(" - ");
        }

        try {
            s6.setValue("" + (data[0] / (data[2] + data[3])));
        } catch (final ArithmeticException e) {
            s6.setValue(" - ");
        }

        try {
            s7.setValue("" + ((data[0] * data[1] - data[2] * data[3]) / (data[0] * data[1] + data[2] * data[3])));
        } catch (final ArithmeticException e) {
            s7.setValue(" - ");
        }
    }
}
